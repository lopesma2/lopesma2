<!-- <style>
html, .root {
    background-color: #252525;
}
</style> -->

<p align="center">
  <a href="https://devops-tech.net.br/" target="_blank" rel="noreferrer"><img src="https://gitlab.com/lopesma2/lopesma2/-/raw/main/images/banner2.png" width=860px alt="my banner"></a>
</p>

<h1> Olá! Meu nome é Marcos Lopes </h1>

💼 DevOps/Cloud Engineer ☁️
<p>
🔭 Interesse em Open Source Software, Cultura DevOps, Infrastructure as Code (IaC), Agile methodologies, Cloud computing.

----

Seja bem vindo ao meu perfil!

Trabalho com tecnologia desde 1999. Iniciei como Técnico de informática durante três anos. Nos anos seguintes atuei na área de Suporte técnico/Helpdesk. Logo após, passei a atuar na área de Administração de redes. Desde de 2019 estou trabalhando com foco em tecnologias de Núvem ou SaaS.


Ao longo dos anos, adquiri uma extensa e sólida experiência ao trabalhar com uma ampla variedade de tecnologias e setores, colaborando com profissionais de diversas áreas. Nos últimos anos, dediquei meu foco à concepção e implementação de aplicações web SaaS com base em sistemas Linux/Unix, em ambientes que abrangem as principais plataformas de nuvem, como AWS, Azure e Google Cloud, além de sistemas On-Premises.

Minha expertise abrange uma ampla gama de responsabilidades, incluindo análise, design técnico de infraestrutura, migração de recursos on-premises para a nuvem, configuração, administração, monitoramento, segurança, suporte, automação de processos essenciais e o desenvolvimento de pipelines de integração e entrega contínua (CI/CD).

----
### Contato:

<div style="display: inline">
  <a href ="mailto:lopesma2@gmail.com"><img src="https://img.shields.io/badge/gmail-lopesma2?style=for-the-badge&logo=gmail&color=gray&link=mailto" target="_blank"></a>
  <a href="https://www.linkedin.com/in/marcos-cosmo-lopes-22585226" target="_blank"><img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white"></a> 
  <a href="https://www.gitlab.com/lopesma2" target="_blank"><img src="https://img.shields.io/badge/gitlab-lopesma2?style=for-the-badge&logo=gitlab&color=gray"></a> 
</div>

----

<table>
    <h3>Tecnologias</h3>
    <thead>
        <tr>
            <th>Sistema</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <img align="center" alt="Windows" height="50" width="70" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/windows8/windows8-original.svg">
            </td>
            <td width="95%" >Windows 98/NT/Vista/XP/20xx. Trabalhei com diversas versões do sistema operacional, incluindo administração e resolução de problemas envolvendo AD, DHCP, DNS, GPO, Print Server e IIS como WebServer para aplicações</td>
        </tr>
        <tr>    
            <td>
                <img align="center" alt="Linux" height="50" width="70" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg">
            </td>
            <td>Desde de 2004 tenho trabalhado com o sistema operacional Linux e diversas distrbuições, sendo elas: Debian (Potato-Blackworm), Ubuntu, RedHat 7.2/8.6/9.0 e Enterprise, Centos, Fedora, Arch Linux e Oracle Linux</td>
        </tr>
        <tr>
            <td style="align: center">
                <img align="center" alt="FreeBSD" height="45" width="65" src="https://raw.githubusercontent.com/gilbarbara/logos/main/logos/freebsd.svg">
            </td>
            <td>FreeBSD 4.6/5.6/6.2-RELEASE em projetos para Firewall de borda, Web Server and Databases Server com Oracle </td>
        </tr>
        <tr>
            <td style="align: center">
                <img align="center" alt="Solaris" height="45" width="65" src="https://upload.wikimedia.org/wikipedia/it/thumb/0/0e/Logo_Solaris_OS.svg/1280px-Logo_Solaris_OS.svg.png">
            </td>
            <td>Solaris Sun em sistemas de telefonia/PBX e Databases Server com Oracle</td>
        </tr>
        <tr>
            <td>
                <img align="center" alt="AWS" height="60" width="80" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/amazonwebservices/amazonwebservices-original-wordmark.svg">
            </td>
            <td>AWS: Desde de 2019 tenho trabalhando com diversos recursos e em projetos, dentre eles: VPC, EC2, ECS em modo Fargate e EC2, EKS, Cloudfront, Route53, Cloudwatch, RDS e outros. </td>
        </tr>
        <tr>
            <td>
                <img align="center" alt="Azure" height="40" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/azure/azure-original.svg">
            </td>
            <td>Azure: Desde de 2019 tenho trabalhando com diversos recursos e em projetos, dentre eles: VNET, VM, AKS, App Service e outros. </td>
        </tr>
        <tr>
            <td>
                <img align="center" alt="MReis-AWS" height="50" width="70" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/googlecloud/googlecloud-original.svg">
            </td>
            <td>GCP: Cloud Virtual Network, Cloud DNS, Persistent Disk, Cloud Storage, Cloud Logging, Stackdriver Monitoring, IAM, Compute Engine e GKE</td>
        </tr>
    </tbody>
<p>
</table>
